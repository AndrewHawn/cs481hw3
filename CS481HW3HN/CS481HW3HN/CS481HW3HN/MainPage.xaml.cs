﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481HW3HN
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void DogClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DogPage());
        }

        async void CatClicked(object sender, EventArgs e)
        {
            string input = await DisplayActionSheet("Literally tap cat",
                null,
                "cat");
            if(input.Equals("cat"))
            {
                await Navigation.PushAsync(new CatPage());
            }
            //Navigation.PushAsync(new CatPage());
        }
    }
}
